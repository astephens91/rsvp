const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000
const url = "mongodb://localhost:27017/rsvp";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
mongoose.Promise = global.Promise;
mongoose.connect(url);

const db = mongoose.connection;

const guestSchema = new mongoose.Schema({
  name: String,
  email: String,
  attending: String,
  numberOfGuests: { type: Number, min: 0, max: 8, }
})

const Guest = mongoose.model("Guest", guestSchema);

app.set('views', './views')
app.set("view engine", "pug");




db.on('error', console.error.bind(console, 'connection error:'));


db.once('open', () => {
  app.listen(port, () =>{
    console.log("RSVP - server listening on port ", port)
  })
  })

  app.get('/', (req, res) => {
    res.render('index')
  })




  app.post("/reply", (req, res) => {
    let myData = new Guest(req.body);
    myData.save()
        .then(item => {
            res.render("reply");
            console.log(item)
        })
        .catch(err => {
            res.status(400).send("Unable to save to database");
        });
});

app.get("/guests", (req, res) =>{
  let goosts1 = Guest.find()
  .then(guestArray => {
    console.log(guestArray)
    res.render("guests", {guestArray})
  })
 
  // res.render("guests")
})